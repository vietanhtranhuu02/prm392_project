package com.example.androidproject.Adapter;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.androidproject.Activity.RecyclerViewInterface;
import com.example.androidproject.Domain.ProductDomain;
import com.example.androidproject.R;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ProductViewHolder>{
    private final List<ProductDomain> mlistProduct;
    RecyclerViewInterface rvInterface;

    public ProductAdapter(List<ProductDomain> mlistProduct, RecyclerViewInterface rvInterface) {
        this.mlistProduct = mlistProduct;
        this.rvInterface = rvInterface;
    }

    @NonNull
    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_item_list,parent,false);
        return new ProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductViewHolder holder, int position) {

        holder.tvName.setText(mlistProduct.get(position).getName());
        holder.tvPrice.setText(String.valueOf(mlistProduct.get(position).getPrice()));
        holder.tvCount.setText(String.valueOf(mlistProduct.get(position).getCount()));
        Picasso.get().load(mlistProduct.get(position).getImage_url()).into(holder.img);
        ProductDomain p = mlistProduct.get(position);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(rvInterface!=null){
                    rvInterface.onItemClick(p);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        if(mlistProduct!=null){
            return mlistProduct.size();
        }
        return 0;
    }

    public static class ProductViewHolder extends RecyclerView.ViewHolder{
        private final TextView tvName, tvPrice, tvCount;
        private final ImageView img;
        public ProductViewHolder(@NonNull View itemView){
            super(itemView);
            tvName = itemView.findViewById(R.id.tv_name);
            tvPrice = itemView.findViewById(R.id.tv_price);
            img = itemView.findViewById(R.id.imgViewProduct);
            tvCount =itemView.findViewById(R.id.countText);
        }
    }
}
