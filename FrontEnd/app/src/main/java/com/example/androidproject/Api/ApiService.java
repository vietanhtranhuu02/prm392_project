package com.example.androidproject.Api;

import com.example.androidproject.Api.LoginApi.LoginRequest;
import com.example.androidproject.Api.RegisterApi.RegisterRequest;
import com.example.androidproject.Api.RegisterApi.RegisterRespone;
import com.example.androidproject.Domain.BillDetail;
import com.example.androidproject.Domain.Cart;
import com.example.androidproject.Domain.ProductDomain;
import com.example.androidproject.Domain.User;
import com.example.androidproject.Model.BillBody;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {
    Gson gson = new GsonBuilder().setDateFormat("dd-MM-yyyy").create();
    ApiService apiService = new Retrofit.Builder()
            .baseUrl("http://10.0.2.2:8080/api/").client(new OkHttpClient().newBuilder().build())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build().create(ApiService.class);
    @GET("home")
    Call<List<ProductDomain>> getListProduct();
    @GET("home/{id}")
    Call<ProductDomain> getProductById(@Path("id") int id);
    @GET("home/cid")
    Call<List<ProductDomain>> getProductByCategoryId(@Query("cid") int cid);

    @GET("cart/userId")
    Call<List<Cart>> getCartByUserId(@Query("userId") int userId);
    @GET("cart/getCart")
    Call<CardRespond> getCartByUserIdAndProductId(@Query("userId") int userId, @Query("productId") int productId);
    @POST("cart/add")
    Call<Void> addToCart(@Body Cart cart);
    @POST("cart/update")
    Call<Void> updateCart(@Body Cart cart);

    @POST("cart/delete")
    Call<Void> deleteCart(@Body Cart cart);
    @POST("cart/deleteByUserId")
    Call<Void> deleteCartByUserId(@Body User user);

    @POST("bill")
    Call<Void> createBill(@Body BillBody billBody);
    @POST("billDetail")
    Call<List<BillDetail>> createBillDetails(@Body BillBody billBody);

    @POST("login")
    Call<User> login(@Body LoginRequest loginRequest);

    @POST("register")
    Call<RegisterRespone> register(@Body RegisterRequest registerRequest);

    @GET("getdata")
    Call<User> getData();
}
