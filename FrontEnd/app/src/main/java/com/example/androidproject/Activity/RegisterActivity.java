package com.example.androidproject.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.androidproject.Api.ApiService;
import com.example.androidproject.Api.RegisterApi.RegisterRequest;
import com.example.androidproject.Api.RegisterApi.RegisterRespone;
import com.example.androidproject.R;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {
    EditText name,email,password,fullName;
    Button btn_signup,btn_back;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        Intent intent = new Intent(this, LoginActivity.class);

        name = (EditText) findViewById(R.id.username);
        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        fullName = (EditText) findViewById(R.id.fullname);
        btn_signup =(Button)findViewById(R.id.btn_signup);
        btn_back =(Button)findViewById(R.id.btn_back);
        btn_signup.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                RegisterRequest registerRequest = new RegisterRequest();
                registerRequest.setUserName(name.getText().toString());
                registerRequest.setFullName(fullName.getText().toString());
                registerRequest.setEmail(email.getText().toString());
                registerRequest.setPassword(password.getText().toString());
                registerUser(registerRequest);
            }
        });
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(RegisterActivity.this,LoginActivity.class));
            }
        });

    }


    public void registerUser(RegisterRequest registerRequest){
        Call<RegisterRespone> registerResponeCall = ApiService.apiService.register(registerRequest);
        registerResponeCall.enqueue(new Callback<RegisterRespone>() {
            @Override
            public void onResponse(Call<RegisterRespone> call, Response<RegisterRespone> response) {
                if(response.isSuccessful()){
                    String mess = "Register Successfully";
                    Toast.makeText(RegisterActivity.this, mess, Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(RegisterActivity.this,LoginActivity.class));
                    finish();
                }else{
                    String mess = "An error occurred, try again";
                    Toast.makeText(RegisterActivity.this, mess, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<RegisterRespone> call, Throwable t) {
                Toast.makeText(RegisterActivity.this, "Register Fail", Toast.LENGTH_SHORT).show();
            }
        });

    }
}
