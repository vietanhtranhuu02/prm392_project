package com.example.androidproject.Activity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.example.androidproject.R;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.List;

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback {
    private static final int REQUEST_LOCATION_PERMISSION = 1;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;

    private GoogleMap myMap;
    private FusedLocationProviderClient fusedLocationClient;
    private LocationCallback locationCallback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    // Cập nhật vị trí của người dùng trên bản đồ
//                    updateLocationOnMap(location);
                }
            }
        };

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_LOCATION_PERMISSION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Quyền đã được cấp, tiến hành các nhiệm vụ liên quan đến vị trí
                startLocationUpdates();
            } else {
                // Quyền bị từ chối, xử lý tương ứng (ví dụ: hiển thị thông báo hoặc tắt tính năng vị trí)
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }


    @Override
    public void onMapReady(@NonNull GoogleMap googleMap) {
        myMap = googleMap;

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            // Quyền đã được cấp, bắt đầu cập nhật vị trí
            startLocationUpdates();
        } else {
            // Nếu quyền chưa được cấp, yêu cầu quyền từ người dùng
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    LOCATION_PERMISSION_REQUEST_CODE);
        }

        LatLng haNoi = new LatLng(21.0285, 105.8542);
        myMap.addMarker(new MarkerOptions().position(haNoi).title("HaNoi").snippet("VietNam"));
//        myMap.moveCamera(CameraUpdateFactory.newLatLng(haNoi));

        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(haNoi) // Sets the center of the map to the marker's location
                .zoom(12) // Sets the zoom level (you can adjust this value)
                .build();

        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));


//        List<LatLng> polylinePoints = new ArrayList<>();
//        polylinePoints.add(new LatLng(21.0285, 105.8542)); // Điểm 1 (Hà Nội, Việt Nam)
//        polylinePoints.add(new LatLng(16.0544, 108.2022)); // Điểm 2 (Đà Nẵng, Việt Nam)
//
//        PolylineOptions polylineOptions = new PolylineOptions()
//                .addAll(polylinePoints)
//                .width(5) // Độ rộng của đường thẳng trong pixel
//                .color(Color.BLUE); // Màu của đường thẳng

//        googleMap.addPolyline(polylineOptions);
    }

    private void startLocationUpdates() {
        com.google.android.gms.location.LocationRequest locationRequest = com.google.android.gms.location.LocationRequest.create()
                .setPriority(com.google.android.gms.location.LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10000) // Khoảng cách giữa các cập nhật vị trí trong mili giây
                .setFastestInterval(5000); // Khoảng cách giữa các cập nhật vị trí nhanh nhất trong mili giây


        // Yêu cầu cập nhật vị trí
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);
    }
    private void updateLocationOnMap(Location location) {
        if (myMap != null) {
            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());

            // Xóa tất cả các đánh dấu trước đó trên bản đồ (nếu cần)
            myMap.clear();

            // Tạo một đánh dấu mới tại vị trí người dùng
            MarkerOptions markerOptions = new MarkerOptions()
                    .position(latLng)
                    .title("Vị trí hiện tại");
            myMap.addMarker(markerOptions);

            // Di chuyển camera đến vị trí người dùng và phóng to bản đồ (nếu cần)
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 15); // 15 là mức phóng to
            myMap.animateCamera(cameraUpdate);
        }
    }
}
