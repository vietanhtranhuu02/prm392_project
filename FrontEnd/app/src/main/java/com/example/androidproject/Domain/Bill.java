package com.example.androidproject.Domain;

import java.util.List;

public class Bill {
    int id;

    Float totalPrice;

    User user;

    List<BillDetail> billDetails;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public Float getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Float totalPrice) {
        this.totalPrice = totalPrice;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<BillDetail> getBillDetails() {
        return billDetails;
    }

    public void setBillDetails(List<BillDetail> billDetails) {
        this.billDetails = billDetails;
    }
}
