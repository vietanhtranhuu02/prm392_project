package com.example.androidproject.Api.LoginApi;

public class LoginRequest {
    private String userName;
    private String password;

    public LoginRequest() {
        this.userName = userName;
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String username) {
        this.userName = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
