package com.example.androidproject.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.androidproject.Adapter.CartAdapter;
import com.example.androidproject.Api.ApiService;
import com.example.androidproject.Domain.Bill;
import com.example.androidproject.Domain.Cart;
import com.example.androidproject.Domain.EventBus.TotalPriceEvent;
import com.example.androidproject.Domain.User;
import com.example.androidproject.Model.BillBody;
import com.example.androidproject.Model.BillDetailBody;
import com.example.androidproject.R;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CartActivity extends AppCompatActivity {
    private static List<Cart> cartList;
    private RecyclerView recyclerView;
    private CartAdapter cartAdapter;
    private static float totalAmount = 0.0f;

    private boolean isBill;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

        recyclerView = findViewById(R.id.recyclerview);
        GridLayoutManager layoutManager = new GridLayoutManager(this, 2); // 2 cột
        recyclerView.setLayoutManager(layoutManager);
        cartList = new ArrayList<>();




        SharedPreferences preferences = getSharedPreferences("MyPrefs", MODE_PRIVATE);
        String json = preferences.getString("user", "");
        Gson gson = new Gson();
        User user = gson.fromJson(json, User.class);

        Button payment = findViewById(R.id.payment);
        payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isBill = true;
                List<BillDetailBody> list = new ArrayList<>();
                for (Cart c : cartList) {
                    BillDetailBody billDetailBody = new BillDetailBody();
                    billDetailBody.setProductId(c.getProduct().getId());
                    billDetailBody.setQuantity(c.getQuantity());
                    list.add(billDetailBody);
                }
                Bill bill = new Bill();
                bill.setId(1);
                bill.setTotalPrice(totalPrice());
                BillBody billBody = new BillBody();
                billBody.setBillDetailBodies(list);
                billBody.setBill(bill);
                billBody.setUser(user);
                ApiService.apiService.createBill(billBody).enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(Call<Void> call, Response<Void> response) {
                        Toast.makeText(CartActivity.this, "Payment successfully", Toast.LENGTH_SHORT).show();

                    }

                    @Override
                    public void onFailure(Call<Void> call, Throwable t) {
                    }
                });
                ApiService.apiService.deleteCartByUserId(user).enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(Call<Void> call, Response<Void> response) {
                        if (response.isSuccessful()) {

                        }
                    }

                    @Override
                    public void onFailure(Call<Void> call, Throwable t) {
                        System.out.println(t);
                        Toast.makeText(CartActivity.this, "Something went wrong buy now", Toast.LENGTH_SHORT).show();
                    }
                });
//                Intent intent = new Intent(CartActivity.this,MainActivity.class);
//                startActivity(intent);
            }
        });


        ApiService.apiService.getCartByUserId(user.getId()).enqueue(new Callback<List<Cart>>() {
            @Override
            public void onResponse(Call<List<Cart>> call, Response<List<Cart>> response) {
                if (response.isSuccessful()) {
                    cartList = response.body();
                    cartAdapter = new CartAdapter(cartList);
                    recyclerView.setAdapter(cartAdapter);
                    TextView totalTxt = findViewById(R.id.totalTxt);
                    totalTxt.setText("$" + totalPrice());

                } else {
                    Toast.makeText(CartActivity.this, "Error: " + response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<Cart>> call, Throwable t) {
                Toast.makeText(CartActivity.this, "Error: " + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });


        ItemTouchHelper.SimpleCallback itemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                int position = viewHolder.getAdapterPosition();
                Cart cart = cartList.get(position);
                cartList.remove(cartList.get(position));
                cartAdapter.notifyDataSetChanged();
                ApiService.apiService.deleteCart(cart).enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(Call<Void> call, Response<Void> response) {
                        if (response.code() == 200) {
                            Toast.makeText(CartActivity.this, "Delete success", Toast.LENGTH_LONG).show();
                        } else if (response.code() == 409) {
                            Toast.makeText(CartActivity.this, "Deletee fail!", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Void> call, Throwable t) {
                        System.out.println(t);
                    }
                });
                TextView totalTxt = findViewById(R.id.totalTxt);
                totalTxt.setText("$" + totalPrice());


            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(itemTouchCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);



        ImageView back = findViewById(R.id.backBtn);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CartActivity.this,MainActivity.class);
                startActivity(intent);
            }
        });



    }

private static float totalPrice(){
    totalAmount =0.0f;
    for (Cart cart : cartList) {
        totalAmount += (cart.getProduct().getPrice()) * cart.getQuantity();
    }
    return totalAmount;
}




@Override
    protected void onStart(){
        super.onStart();
        EventBus.getDefault().register(this);
}
@Override
    protected void onStop(){
        EventBus.getDefault().unregister(this);
    super.onStop();
}

@Subscribe(sticky = true,threadMode = ThreadMode.MAIN)
    public void eventTotalPrice(TotalPriceEvent event){
        if(event != null){
            TextView totalTxt = findViewById(R.id.totalTxt);

            totalTxt.setText("$" + totalPrice());
        }
}

}