package com.example.androidproject.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.example.androidproject.Activity.ImgClickListener;
import com.example.androidproject.Activity.RecyclerViewInterface;
import com.example.androidproject.Api.ApiService;
import com.example.androidproject.Domain.Cart;
import com.example.androidproject.Domain.EventBus.TotalPriceEvent;
import com.example.androidproject.R;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.CartHolder>{
    private final List<Cart> listCart;
    RecyclerViewInterface rvInterface;

    public CartAdapter(List<Cart> listCart) {
        this.listCart = listCart;
    }

    @NonNull
    @Override
    public CartHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_item_cart,parent,false);
        return new CartHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CartHolder holder, int position) {

        holder.tvName.setText(listCart.get(position).getProduct().getName());
        holder.tvPrice.setText(String.valueOf(listCart.get(position).getProduct().getPrice()));
        holder.tvCount.setText(String.valueOf(listCart.get(position).getQuantity()));
        Picasso.get().load(listCart.get(position).getProduct().getImage_url()).into(holder.img);

        holder.imgPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int currentPosition = holder.getAdapterPosition();
                if (currentPosition != RecyclerView.NO_POSITION) {
                    int newQuantity = listCart.get(currentPosition).getQuantity() + 1;
                    listCart.get(currentPosition).setQuantity(newQuantity);
                    holder.tvCount.setText(String.valueOf(newQuantity));

                    Cart cart = listCart.get(currentPosition);
                    ApiService.apiService.addToCart(cart).enqueue(new Callback<Void>() {
                        @Override
                        public void onResponse(Call<Void> call, Response<Void> response) {
                           response.isSuccessful();
                        }

                        @Override
                        public void onFailure(Call<Void> call, Throwable t) {
                        }
                    });
                }
                EventBus.getDefault().postSticky(new TotalPriceEvent());

            }
        });

        holder.imgMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int currentPosition = holder.getAdapterPosition();
                if (currentPosition != RecyclerView.NO_POSITION) {
                    int newQuantity = listCart.get(currentPosition).getQuantity()-1;
                    listCart.get(currentPosition).setQuantity(newQuantity);
                    holder.tvCount.setText(String.valueOf(newQuantity));

                    Cart cart = listCart.get(currentPosition);
                    ApiService.apiService.updateCart(cart).enqueue(new Callback<Void>() {
                        @Override
                        public void onResponse(Call<Void> call, Response<Void> response) {
                            response.isSuccessful();
                        }

                        @Override
                        public void onFailure(Call<Void> call, Throwable t) {
                        }
                    });
                }
                EventBus.getDefault().postSticky(new TotalPriceEvent());

            }
        });
    }

    @Override
    public int getItemCount() {
        if(listCart!=null){
            return listCart.size();
        }
        return 0;
    }

    public static class CartHolder extends RecyclerView.ViewHolder{
        private final TextView tvName, tvPrice, tvCount;
        private final ImageView img, imgPlus,imgMinus;
        ImgClickListener  listener;

        public CartHolder(@NonNull View itemView){
            super(itemView);
            tvName = itemView.findViewById(R.id.tv_name);
            tvPrice = itemView.findViewById(R.id.tv_price);
            img = itemView.findViewById(R.id.imgViewProduct);
            tvCount =itemView.findViewById(R.id.countText);
            imgPlus =itemView.findViewById(R.id.imagePlus);
            imgMinus =itemView.findViewById(R.id.imageMinus);


        }

    }


    public static class SwipeToDeleteCallback extends ItemTouchHelper.SimpleCallback {
        private CartAdapter adapter;

        public SwipeToDeleteCallback(CartAdapter adapter) {
            super(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT);
            this.adapter = adapter;
        }

        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {

        }
    }


}
