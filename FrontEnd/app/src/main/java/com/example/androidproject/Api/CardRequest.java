package com.example.androidproject.Api;

public class CardRequest {
    public int userId;
    public int productId;

    public CardRequest() {

    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getProductId(int id) {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public CardRequest(int userId, int productId) {
        this.userId = userId;
        this.productId = productId;
    }
}
