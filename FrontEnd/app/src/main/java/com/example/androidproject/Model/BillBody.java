package com.example.androidproject.Model;

import com.example.androidproject.Domain.Bill;
import com.example.androidproject.Domain.User;

import java.util.List;

public class BillBody {

    private Bill bill;
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    private List<BillDetailBody> billDetailBodies;

    public Bill getBill() {
        return bill;
    }

    public void setBill(Bill bill) {
        this.bill = bill;
    }

    public List<BillDetailBody> getBillDetailBodies() {
        return billDetailBodies;
    }

    public void setBillDetailBodies(List<BillDetailBody> billDetailBodies) {
        this.billDetailBodies = billDetailBodies;
    }

}