package com.example.androidproject.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.androidproject.Api.ApiService;
import com.example.androidproject.Api.LoginApi.LoginRequest;
import com.example.androidproject.Api.LoginApi.LoginRespone;
import com.example.androidproject.Domain.User;
import com.example.androidproject.R;
import com.google.gson.Gson;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {
    EditText nameTxt,passwordTxt;
    Button btn_login, btn_register;
    List<Map<String,String>> user = new ArrayList<>();
    Map<String,String> em = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Intent intent = getIntent();
        Map<String, String> em = (Map<String, String>) intent.getSerializableExtra("user_data");

        nameTxt = (EditText) findViewById(R.id.username);
        passwordTxt = (EditText) findViewById(R.id.password);
        btn_login = (Button) findViewById(R.id.btn_login);
        btn_register = (Button) findViewById(R.id.btn_register);


        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(TextUtils.isEmpty(nameTxt.getText().toString()) || TextUtils.isEmpty(passwordTxt.getText().toString())){
                    Toast.makeText(LoginActivity.this,"Username/Password Required",Toast.LENGTH_SHORT).show();
                }else{
                    callApi();
                }

            }
        });
        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this,RegisterActivity.class));
            }
        });
    }

    public void callApi(){
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setUserName(nameTxt.getText().toString());
        loginRequest.setPassword(passwordTxt.getText().toString());


        Call<User> loginCall = ApiService.apiService.login(loginRequest);
        loginCall.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if(response.isSuccessful()){
                    Gson gson = new Gson();
                    User user = response.body();
                    String json = gson.toJson(user);
                    Toast.makeText(LoginActivity.this, "Login Success", Toast.LENGTH_SHORT).show();
//                    new Handler().postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            startActivity(new Intent(LoginActivity.this,MainActivity.class).putExtra("data",user));
//                        }
//                    },700);

                            startActivity(new Intent(LoginActivity.this,MainActivity.class));

                    SharedPreferences preferences = getSharedPreferences("MyPrefs", MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString("user", json );
                    editor.apply();

                }else{
                    Toast.makeText(LoginActivity.this, "Login fail", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Toast.makeText(LoginActivity.this, "Throwable" + t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }
}
