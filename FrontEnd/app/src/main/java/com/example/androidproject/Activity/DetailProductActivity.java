package com.example.androidproject.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.androidproject.Adapter.CartAdapter;
import com.example.androidproject.Api.ApiService;
import com.example.androidproject.Api.CardRequest;
import com.example.androidproject.Api.CardRespond;
import com.example.androidproject.Domain.Cart;
import com.example.androidproject.Domain.ProductDomain;
import com.example.androidproject.Domain.User;
import com.example.androidproject.R;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailProductActivity extends AppCompatActivity {

    private static final int REQUEST_CALL_PERMISSION = 1;
    private static final String CHANNEL_ID = "my_channel_id";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_product);
        Intent intent = getIntent();
        ProductDomain product = (ProductDomain) intent.getSerializableExtra("product");
        TextView productNameTextView = findViewById(R.id.name);
        TextView productPriceTextView = findViewById(R.id.price);
        ImageView productImageView = findViewById(R.id.imageview);
        TextView desTextView = findViewById(R.id.des);
        TextView count = findViewById(R.id.count);
        Button btn_back = findViewById(R.id.btn_back);
        Button btn_call = findViewById(R.id.btn_call);
        Button btn_buy =findViewById(R.id.btn_buy);

        productNameTextView.setText(product.getName());
        productPriceTextView.setText(String.valueOf(product.getPrice()));
        Picasso.get().load(product.getImage_url()).into(productImageView);
        desTextView.setText(product.getDescription());
        count.setText(String.valueOf(product.getCount()));

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DetailProductActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, REQUEST_CALL_PERMISSION);
        }
        btn_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:0395873238"));
                if (checkSelfPermission(android.Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                    startActivity(intent);
                }
            }
        });
        SharedPreferences preferences = getSharedPreferences("MyPrefs", MODE_PRIVATE);
        String json = preferences.getString("user", "");
        Gson gson = new Gson();
        User user = gson.fromJson(json, User.class);

//
//        CardRequest cardRequest = new CardRequest();
//        cardRequest.setUserId(user.getId());
//        cardRequest.getProductId(product.getId());

        Cart cart = new Cart();
        ApiService.apiService.getCartByUserIdAndProductId(user.getId(),product.getId()).enqueue(new Callback<CardRespond>() {
            @Override
            public void onResponse(Call<CardRespond> call, Response<CardRespond> response) {
                if(response.isSuccessful()){
                    cart.setProduct(response.body().getProduct());
                    cart.setUserId(response.body().getUserId());
                    cart.setQuantity(response.body().getQuantity());
                }
            }

            @Override
            public void onFailure(Call<CardRespond> call, Throwable t) {
                Toast.makeText(DetailProductActivity.this, "Throwable" + t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        btn_buy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(cart.getProduct() == null){
                    cart.setUserId(user.getId());
                    cart.setProduct(product);
                    cart.setQuantity(1);
                }

                ApiService.apiService.addToCart(cart).enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(Call<Void> call, Response<Void> response) {
                        if (response.isSuccessful()) {
                            createNotificationChannel();
                            showNotification();

                        } else {
                            Toast.makeText(DetailProductActivity.this, "Error: " + response.message(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Void> call, Throwable t) {
                        Toast.makeText(DetailProductActivity.this, "Error: " + t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }


    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String channelId = "my_channel_id";
            CharSequence channelName = "My Channel";
            String channelDescription = "My Channel Description";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;

            NotificationChannel channel = new NotificationChannel(channelId, channelName, importance);
            channel.setDescription(channelDescription);

            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }


    private void showNotification() {
        Intent intent = new Intent(this, CartActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_IMMUTABLE);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_notification_foreground)
                .setContentTitle("Bạn có thông báo mới ")
                .setContentText("Bạn đã thêm sản phẩm vào giỏ hàng")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.POST_NOTIFICATIONS) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        notificationManager.notify(1, builder.build());
    }

}