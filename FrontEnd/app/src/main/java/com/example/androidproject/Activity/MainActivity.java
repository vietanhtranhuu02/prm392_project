package com.example.androidproject.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.androidproject.Adapter.CartAdapter;
import com.example.androidproject.Adapter.ProductAdapter;
import com.example.androidproject.Api.ApiService;
import com.example.androidproject.Domain.Cart;
import com.example.androidproject.Domain.ProductDomain;
import com.example.androidproject.Domain.User;
import com.example.androidproject.R;
import com.google.gson.Gson;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    TextView username;
    private RecyclerView recyclerView;
    private List<ProductDomain> mListProduct;

    private List<Cart> cartList;

    private static final String CHANNEL_ID = "my_channel_id";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        username = findViewById(R.id.textView2);
//        User user = (User) getIntent().getSerializableExtra("data");
        SharedPreferences preferences = getSharedPreferences("MyPrefs", MODE_PRIVATE);
        String json = preferences.getString("user", "");
        Gson gson = new Gson();
        User user = gson.fromJson(json, User.class);

        if(user != null){
            username.setText(user.getUserName());
        }
//        Intent intent = getIntent();
//        if(intent.getExtras() !=null ){
//            String name = intent.getStringExtra("data");
//            username.setText(name);
//        }
        recyclerView = findViewById(R.id.view);
        GridLayoutManager layoutManager = new GridLayoutManager(this, 2); // 2 cột
        recyclerView.setLayoutManager(layoutManager);
        LinearLayout change_map = findViewById(R.id.change_map);
        LinearLayout cart_bt = findViewById(R.id.cart_onclick);

        cart_bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, CartActivity.class);
                startActivity(intent);
                finish();
            }
        });
        change_map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, MapActivity.class);
                startActivity(intent);
                finish();
            }
        });
        mListProduct = new ArrayList<>();
        callApiGetAllProduct();
        LinearLayout shirt = findViewById(R.id.shirt_cate);
        LinearLayout pants = findViewById(R.id.pants_cate);
        LinearLayout acc = findViewById(R.id.acc_cate);
        LinearLayout all = findViewById(R.id.all_cate);
        shirt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callApiGetListProductByCategoryId1();
            }
        });
        pants.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callApiGetListProductByCategoryId2();
            }
        });
        acc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callApiGetListProductByCategoryId3();
            }
        });
        all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callApiGetAllProduct();
            }
        });
        TextView seeall = findViewById(R.id.see_all);
        seeall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callApiGetAllProduct();
            }
        });
        LinearLayout exit = findViewById(R.id.exit);
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });

        cartList = new ArrayList<>();
        ApiService.apiService.getCartByUserId(user.getId()).enqueue(new Callback<List<Cart>>() {
            @Override
            public void onResponse(Call<List<Cart>> call, Response<List<Cart>> response) {
                if (response.isSuccessful()) {
                    cartList = response.body();
                    if(cartList.size() > 0){
                            createNotificationChannel();
                            showNotification();
                    }

                }
            }

            @Override
            public void onFailure(Call<List<Cart>> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Error: " + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void callApiGetAllProduct(){
        RecyclerViewInterface recyclerViewInterface = new RecyclerViewInterface() {
            @Override
            public void onItemClick(ProductDomain productDomain) {
                Intent intent = new Intent(MainActivity.this, DetailProductActivity.class);
                intent.putExtra("product", (Serializable) productDomain);
                startActivity(intent);
            }
        };
        ApiService.apiService.getListProduct().enqueue((new Callback<List<ProductDomain>>() {
            @Override
            public void onResponse(Call<List<ProductDomain>> call, Response<List<ProductDomain>> response) {
                mListProduct = response.body();
                ProductAdapter productAdapter = new ProductAdapter(mListProduct, recyclerViewInterface);
                recyclerView.setAdapter(productAdapter);
            }

            @Override
            public void onFailure(Call<List<ProductDomain>> call, Throwable t) {
                Toast.makeText(MainActivity.this, "onFailure", Toast.LENGTH_SHORT).show();
            }
        }));

    }
    private void callApiGetListProductByCategoryId1(){
        RecyclerViewInterface recyclerViewInterface = new RecyclerViewInterface() {
            @Override
            public void onItemClick(ProductDomain productDomain) {
                Intent intent = new Intent(MainActivity.this, DetailProductActivity.class);
                intent.putExtra("product", (Serializable) productDomain);
                startActivity(intent);
            }
        };
        ApiService.apiService.getProductByCategoryId(1).enqueue(new Callback<List<ProductDomain>>() {
            @Override
            public void onResponse(Call<List<ProductDomain>> call, Response<List<ProductDomain>> response) {
                mListProduct = response.body();
                ProductAdapter productAdapter = new ProductAdapter(mListProduct, recyclerViewInterface);
                recyclerView.setAdapter(productAdapter);
            }

            @Override
            public void onFailure(Call<List<ProductDomain>> call, Throwable t) {
                Toast.makeText(MainActivity.this, "onFailure", Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void callApiGetListProductByCategoryId2(){
        RecyclerViewInterface recyclerViewInterface = new RecyclerViewInterface() {
            @Override
            public void onItemClick(ProductDomain productDomain) {
                Intent intent = new Intent(MainActivity.this, DetailProductActivity.class);
                intent.putExtra("product", (Serializable) productDomain);
                startActivity(intent);
            }
        };
        ApiService.apiService.getProductByCategoryId(2).enqueue(new Callback<List<ProductDomain>>() {
            @Override
            public void onResponse(Call<List<ProductDomain>> call, Response<List<ProductDomain>> response) {
                mListProduct = response.body();
                ProductAdapter productAdapter = new ProductAdapter(mListProduct, recyclerViewInterface);
                recyclerView.setAdapter(productAdapter);
            }
            @Override
            public void onFailure(Call<List<ProductDomain>> call, Throwable t) {
                Toast.makeText(MainActivity.this, "onFailure", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void callApiGetListProductByCategoryId3(){
        RecyclerViewInterface recyclerViewInterface = new RecyclerViewInterface() {
            @Override
            public void onItemClick(ProductDomain productDomain) {
                Intent intent = new Intent(MainActivity.this, DetailProductActivity.class);
                intent.putExtra("product", (Serializable) productDomain);
                startActivity(intent);
            }
        };
        ApiService.apiService.getProductByCategoryId(3).enqueue(new Callback<List<ProductDomain>>() {
            @Override
            public void onResponse(Call<List<ProductDomain>> call, Response<List<ProductDomain>> response) {
                mListProduct = response.body();
                ProductAdapter productAdapter = new ProductAdapter(mListProduct, recyclerViewInterface);
                recyclerView.setAdapter(productAdapter);
            }
            @Override
            public void onFailure(Call<List<ProductDomain>> call, Throwable t) {
                Toast.makeText(MainActivity.this, "onFailure", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String channelId = "my_channel_id";
            CharSequence channelName = "My Channel";
            String channelDescription = "My Channel Description";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;

            NotificationChannel channel = new NotificationChannel(channelId, channelName, importance);
            channel.setDescription(channelDescription);

            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }


    private void showNotification() {
        Intent intent = new Intent(this, CartActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_IMMUTABLE);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_notification_foreground)
                .setContentTitle("Bạn có thông báo mới ")
                .setContentText("Trong giỏ hàng có sản phẩm chưa được thanh toán")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.POST_NOTIFICATIONS) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        notificationManager.notify(1, builder.build());
    }

}