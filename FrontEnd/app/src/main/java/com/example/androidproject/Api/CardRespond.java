package com.example.androidproject.Api;

import com.example.androidproject.Domain.ProductDomain;

public class CardRespond {
    private int cartId;
    private int userId;

    private int quantity;
    private ProductDomain product;

    public CardRespond(int cartId, int userId, int quantity, ProductDomain product) {
        this.cartId = cartId;
        this.userId = userId;
        this.quantity = quantity;
        this.product = product;
    }

    public int getCartId() {
        return cartId;
    }

    public void setCartId(int cartId) {
        this.cartId = cartId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public ProductDomain getProduct() {
        return product;
    }

    public void setProduct(ProductDomain product) {
        this.product = product;
    }
}
