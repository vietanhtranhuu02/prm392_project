package com.example.androidproject.Activity;

import com.example.androidproject.Adapter.CartAdapter;
import com.example.androidproject.Domain.Cart;
import com.example.androidproject.Domain.ProductDomain;

public interface RecyclerViewInterface {
    void onItemClick(ProductDomain productDomain);

}
