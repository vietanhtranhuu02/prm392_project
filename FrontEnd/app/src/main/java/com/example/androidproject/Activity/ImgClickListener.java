package com.example.androidproject.Activity;

import android.view.View;

public interface ImgClickListener {
    void  onImgClick(View view, int position, int value);
}
