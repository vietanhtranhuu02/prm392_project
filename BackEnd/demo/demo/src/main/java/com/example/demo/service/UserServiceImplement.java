package com.example.demo.service;

import com.example.demo.api.model.LoginBody;
import com.example.demo.api.model.RegistrationBody;
import com.example.demo.entity.User;
import com.example.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImplement implements IUserService{

    @Autowired
    private UserRepository userRepository;

    @Override
    public User addUser(User user) {
        if(user != null){
            return userRepository.save(user);
        }
        return null;
    }

    @Override
    public User register(@Valid RegistrationBody registrationBody) throws Exception {
        if (userRepository.findByUserNameIgnoreCase(registrationBody.getUserName()).isPresent())
        {
            throw new Exception();
        }

        User user = new User();
        user.setUserName(registrationBody.getUserName());
        user.setFullName(registrationBody.getFullName());
        user.setEmail(registrationBody.getEmail());
        user.setPassword((registrationBody.getPassword()));
        return userRepository.save(user);
    }

    public Boolean login(@Valid LoginBody loginBody) {
        Optional<User> optionalUser = userRepository.findByUserNameIgnoreCase(loginBody.getUserName());
        if (optionalUser.isPresent()) {
            User user = optionalUser.get();
            if(user.getPassword().equals(loginBody.getPassword())){
                return true;
            }
        }
        return false;
    }

    @Override
    public List<User> getAllUser() {
        return userRepository.findAll();
    }

    @Override
    public User getUserById(long id) {
        return userRepository.getById(id);
    }
}
