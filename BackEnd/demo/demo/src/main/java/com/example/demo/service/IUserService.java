package com.example.demo.service;

import com.example.demo.api.model.LoginBody;
import com.example.demo.api.model.RegistrationBody;
import com.example.demo.entity.User;

import java.util.List;

public interface IUserService {
    public User addUser(User user);

    public User register(RegistrationBody body) throws Exception;

    public Boolean login(LoginBody loginBody);

    public List<User> getAllUser();

    public User getUserById(long id);
}
