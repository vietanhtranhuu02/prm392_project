package com.example.demo.api.model;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class CartBody {
    private int userId;
    private int productId;
}
