package com.example.demo.service;

import com.example.demo.entity.Category;
import com.example.demo.entity.Product;

import java.util.List;

public interface ICategoryService {
    public List<Category> getAllCategory();

}
