package com.example.demo.service.Bill;

import com.example.demo.api.model.BillBody;
import com.example.demo.api.model.BillDetailBody;
import com.example.demo.entity.Bill;
import com.example.demo.entity.BillDetail;
import com.example.demo.entity.Product;
import com.example.demo.repository.BillDetailRepository;
import com.example.demo.repository.BillRepository;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.ProductServiceImplement;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class BillService {
    private final BillRepository billRepository;
    private final ProductServiceImplement productServiceImplement;
    private final BillDetailRepository billDetailRepository;
    private final UserRepository userRepository;

    public BillService(BillRepository _billRepository, ProductServiceImplement productServiceImplement, BillDetailRepository billDetailRepository, UserRepository userRepository) {
        this.billRepository = _billRepository;
        this.productServiceImplement = productServiceImplement;
        this.billDetailRepository = billDetailRepository;
        this.userRepository = userRepository;
    }

    @Transactional
    public void createBill(BillBody body) {
        Bill bill = new Bill();
        bill.setUser(body.getUser());
        bill.setTotalPrice(body.getBill().getTotalPrice());
        billRepository.save(bill);
        List<BillDetail> billDetails = new ArrayList<>();
        if (bill != null) {
            for (BillDetailBody billDetailBody : body.getBillDetailBodies()) {
                billDetails.add(createBillDetail(bill, billDetailBody));
            }
        }
        billDetailRepository.saveAll(billDetails);
    }
    public BillDetail createBillDetail(Bill bill, BillDetailBody billDetailBody) {
        BillDetail billDetail = new BillDetail();
        billDetail.setBill(bill);
        billDetail.setQuantity(billDetailBody.getQuantity());
        Product product = productServiceImplement.getProductById(billDetailBody.getProductId());
        billDetail.setProduct(product);

        return billDetail;
    }

    public Bill getBillById(int id) {
        Optional<Bill> bill = billRepository.findById(id);
        return bill.orElse(null);
    }
}
