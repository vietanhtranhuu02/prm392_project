package com.example.demo.service;

import com.example.demo.entity.Product;

import java.util.List;

public interface IProductService {
    public List<Product> getAllProduct();

    public Product getProductById(int id);

    public List<Product> getProductByCategory(int cid);
}
