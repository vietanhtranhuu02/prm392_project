package com.example.demo.api.controller;

import com.example.demo.api.model.BillBody;
import com.example.demo.entity.BillDetail;
import com.example.demo.service.BillDetail.BillDetailService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/billDetail")
public class BillDetailController {

    private final BillDetailService billDetailService;

    public BillDetailController(BillDetailService _billDetailService) {
        this.billDetailService = _billDetailService;
    }

    @PostMapping
    public List<BillDetail> createBillDetails(@RequestBody BillBody body) {
        return billDetailService.createBillDetails(body.getBill().getId(), body.getBillDetailBodies());
    }

}
