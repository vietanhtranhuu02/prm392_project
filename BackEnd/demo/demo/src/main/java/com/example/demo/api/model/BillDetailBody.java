package com.example.demo.api.model;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class BillDetailBody {
    @NotNull
    private int productId;
    @NotNull
    private int quantity;
}
