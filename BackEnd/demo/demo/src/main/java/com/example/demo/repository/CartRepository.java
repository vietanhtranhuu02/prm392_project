package com.example.demo.repository;

import com.example.demo.api.model.CartBody;
import com.example.demo.entity.Cart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Repository
public interface CartRepository extends JpaRepository<Cart, Integer> {
    List<Cart> findByUserId(int userId);

    Cart findByUserIdAndProduct_Id(int userId, int productId);

//    Cart findByUserIdAndProduct_Id(CartBody cartBody);

    @Modifying
    @Transactional
    @Query(value = "DELETE FROM gdxbsqz20ipx52bo.cart WHERE user_id = :userId", nativeQuery = true)
    int deleteByUserId( Integer userId);

    // Thêm các phương thức truy vấn và xử lý dữ liệu khác tại đây
}

