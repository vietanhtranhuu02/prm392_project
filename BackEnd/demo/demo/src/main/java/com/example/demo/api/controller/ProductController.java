package com.example.demo.api.controller;

import com.example.demo.entity.Cart;
import com.example.demo.entity.Product;
import com.example.demo.repository.ProductRepository;
import com.example.demo.service.IProductService;
import com.example.demo.service.ProductServiceImplement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/home")
public class ProductController {
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private IProductService iProductService;
    @Autowired
    private ProductServiceImplement productService;
    @GetMapping()
    public List<Product> getAllProduct(){
        return iProductService.getAllProduct();
    }
    @GetMapping("/{id}")
    public Product getProductById(@PathVariable("id") int id){
        return productService.getProductById(id);
    }
    @GetMapping("/cid")
    public List<Product> GetProductByCategory(@RequestParam Integer cid){
        return productService.getProductByCategory(cid);
    }


}
