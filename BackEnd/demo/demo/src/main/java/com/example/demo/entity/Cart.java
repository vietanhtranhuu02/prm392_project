package com.example.demo.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Data
@Entity
@Table(name = "cart")
public class Cart {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id",nullable = false)
    private int cartId;

    @Column(name = "userId")
    private int userId;


    @Column(name = "quantity")
    private int quantity;

    @ManyToOne()
    @JoinColumn(name = "product_id")
    private Product product;




}
