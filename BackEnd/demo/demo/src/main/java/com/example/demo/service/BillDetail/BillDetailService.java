package com.example.demo.service.BillDetail;

import com.example.demo.api.model.BillDetailBody;
import com.example.demo.entity.Bill;
import com.example.demo.entity.BillDetail;
import com.example.demo.entity.Product;
import com.example.demo.repository.BillDetailRepository;
import com.example.demo.service.Bill.BillService;
import com.example.demo.service.ProductServiceImplement;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;
@Service
public class BillDetailService {
    private final BillDetailRepository billDetailRepository;
    private final BillService billService;
    private final ProductServiceImplement productServiceImplement;

    public BillDetailService(BillDetailRepository _billDetailRepository,
                             ProductServiceImplement _productServiceImplement,
                             BillService _billService) {
        this.billDetailRepository = _billDetailRepository;
        this.productServiceImplement = _productServiceImplement;
        this.billService = _billService;
    }

    public BillDetail createBillDetail(Bill bill, BillDetailBody billDetailBody) {
        BillDetail billDetail = new BillDetail();
        billDetail.setBill(bill);
        billDetail.setQuantity(billDetailBody.getQuantity());
        Product product = productServiceImplement.getProductById(billDetailBody.getProductId());
        billDetail.setProduct(product);

        return billDetail;
    }

    public List<BillDetail> createBillDetails(int billId, List<BillDetailBody> billDetailBodies) {
        List<BillDetail> billDetails = new ArrayList<>();
        Bill bill = billService.getBillById(billId);
        if (bill != null) {
            for (BillDetailBody billDetailBody : billDetailBodies) {
                billDetails.add(createBillDetail(bill, billDetailBody));
            }
        }

        return billDetailRepository.saveAll(billDetails);
    }
}
