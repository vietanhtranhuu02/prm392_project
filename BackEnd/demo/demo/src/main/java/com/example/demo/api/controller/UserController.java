package com.example.demo.api.controller;

import com.example.demo.api.model.LoginBody;
import com.example.demo.api.model.RegistrationBody;
import com.example.demo.entity.User;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.IUserService;
import com.example.demo.service.UserServiceImplement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.*;

@RestController
@RequestMapping("/api")
public class UserController {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private IUserService iUserService;
    @Autowired
    private UserServiceImplement userService;



    @GetMapping("/a/{email}")
    public Optional<User> test(@PathVariable String email){
        Optional<User> user = userRepository.findByEmail(email);
        return user;
    }


    @GetMapping("/all")
    public List<User> getAllUser(){
        return iUserService.getAllUser();
    }

    @GetMapping("/getdata")
    public ResponseEntity<User> test(HttpSession session){
        User user = (User) session.getAttribute("user");
        return ResponseEntity.ok(user);
    }

    @PostMapping("/add")
    public ResponseEntity<User> addUser(@RequestBody User user){
        return  ResponseEntity.ok(iUserService.addUser(user))  ;
    }
    @PostMapping("/register")
    public ResponseEntity<RegistrationBody> register(@Valid @RequestBody RegistrationBody registrationBody) {
        try {
        iUserService.register(registrationBody);
            return ResponseEntity.ok(registrationBody);
          } catch (Exception e) {
        return ResponseEntity.status(HttpStatus.CONFLICT).build();
        }
}

    @PostMapping("/login")
    public ResponseEntity<User> login(@Valid @RequestBody LoginBody loginBody, HttpServletRequest request) {
        User user = userRepository.findByUserName(loginBody.getUserName());
        if (  iUserService.login(loginBody)) {
            HttpSession sessions =request.getSession();
            sessions.setAttribute("user", user);
            return ResponseEntity.ok(user);
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

}
