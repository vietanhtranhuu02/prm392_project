package com.example.demo.api.controller;

import com.example.demo.api.model.CartBody;
import com.example.demo.entity.Cart;
import com.example.demo.entity.User;
import com.example.demo.service.Cart.ICartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/cart")
public class CartController {

    @Autowired
    private ICartService iCartService;

    @Autowired
    public CartController(ICartService iCartService) {
        this.iCartService = iCartService;
    }
    @GetMapping("/userId")
    public List<Cart> GetCartByUserId(@RequestParam int userId) {
        return iCartService.getCartByUserId(userId);
    }

    @PostMapping("/add")
    public void addToCart(@RequestBody Cart cart){
          iCartService.addToCart(cart)  ;
    }
    @PostMapping("/update")
    public void updateCart(@RequestBody Cart cart){
          iCartService.updateCart(cart);  ;
    }

    @GetMapping("/getCart")
    public Cart getCart(@RequestParam int userId, @RequestParam int productId){
        return iCartService.getCart(userId,productId);
    }

//    @GetMapping("/getCartVa")
//    public ResponseEntity<Cart> getCartVa(@RequestBody CartBody cartBody){
//        return ResponseEntity.ok(iCartService.getCartVa(cartBody));
//    }
    @PostMapping("/delete")
    public void deleteCart(@RequestBody Cart cart){
         iCartService.deleteCart(cart);
    }
    @PostMapping("/deleteByUserId")
    public void deleteCartByUserId(@RequestBody User user){
         iCartService.deleteCartById(user);
    }
}