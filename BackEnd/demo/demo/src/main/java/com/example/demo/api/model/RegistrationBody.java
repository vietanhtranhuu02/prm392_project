package com.example.demo.api.model;

import lombok.Getter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
public class RegistrationBody
{
    @NotNull
    @NotBlank
    @Size(min = 3, max = 255)
    private String userName;
    @NotNull
    @NotBlank
    @Size(min = 6, max = 28)
//    @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$")
    private String password;
    @NotNull
    @NotBlank
    private String fullName;
    @NotNull
    @NotBlank
    private String email;
}

