package com.example.demo.api.model;

import com.example.demo.entity.Bill;
import com.example.demo.entity.User;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@Setter
public class BillBody {
    @NotNull
    private Bill bill;
    @NotNull
    private User user;
    @NotNull
    private List<BillDetailBody> billDetailBodies;
}