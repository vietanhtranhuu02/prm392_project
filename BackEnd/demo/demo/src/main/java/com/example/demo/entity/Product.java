package com.example.demo.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "product")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id",nullable = false)
    private int id;

    @Column(name = "name")
    private String name;
    @Column(name = "price")
    private float price;
    @Column(name = "description")
    private String description;
    @Column(name = "category")
    private int category;
    @Column(name = "count")
    private int count;
    @Column(name = "image_url")
    private String image_url;

//    @OneToMany(mappedBy = "product") // Quan hệ 1-n với đối tượng ở dưới (Person) (1 địa điểm có nhiều người ở)
//    private List<Cart> carts;


}
