package com.example.demo.service.Cart;

import com.example.demo.api.model.CartBody;
import com.example.demo.entity.Cart;
import com.example.demo.entity.User;
import com.example.demo.repository.CartRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CartService implements ICartService {
    private final CartRepository cartRepository;
    @Autowired
    public CartService(CartRepository cartRepository) {
        this.cartRepository = cartRepository;
    }

    @Override
    public List<Cart> getCartByUserId(int userId) {
        return cartRepository.findByUserId(userId);
    }

    @Override
    public Cart getCart(int userId,int productId) {
        return cartRepository.findByUserIdAndProduct_Id(userId,productId);
    }

//    @Override
//    public Cart getCartVa(CartBody cartBody) {
//        return cartRepository.findByUserIdAndProduct_Id(cartBody);
//    }

    @Override
    public void addToCart( Cart cart) {
//        if(cartRepository.findByUserIdAndProduct_Id(cart.getUserId(),cart.getProduct().getId()).isPresent()

       Cart cart1 = cartRepository.findByUserIdAndProduct_Id(cart.getUserId(),cart.getProduct().getId());
        if(cart1 !=null){
            cart1.setQuantity(cart1.getQuantity() +1);
            cartRepository.save(cart1);
        }
       else {
           cartRepository.save(cart);
        }
    }
    public void updateCart( Cart cart) {
       Cart cart1 = cartRepository.findByUserIdAndProduct_Id(cart.getUserId(),cart.getProduct().getId());
        if(cart1 !=null){
            cart1.setQuantity(cart1.getQuantity() -1);
            cartRepository.save(cart1);
        }

    }

    @Override
    public void deleteCartById(User user) {
        cartRepository.deleteByUserId(user.getId());
    }

    public void deleteCart(Cart cart){
         cartRepository.delete(cart);
    }
}