package com.example.demo.service.Cart;

import com.example.demo.api.model.CartBody;
import com.example.demo.entity.Cart;
import com.example.demo.entity.User;

import java.util.List;

public interface ICartService {
        List<Cart> getCartByUserId(int userId);

        void addToCart(Cart cart);

        Cart getCart(int userId, int productId);

//        Cart getCartVa(CartBody cartBody);
        void deleteCart(Cart cart);

        void updateCart(Cart cart);

        void deleteCartById(User user);


}
