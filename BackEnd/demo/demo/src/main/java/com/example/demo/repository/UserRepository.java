package com.example.demo.repository;

import com.example.demo.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.swing.text.html.Option;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByUserNameIgnoreCase(String username);
    Optional<User> findByEmail(String email);
    User findById(long id);
    Optional<User> findByUserNameOrEmail(String username, String email);
   User findByUserName(String username);
    Boolean existsByUserName(String username);
    Boolean existsByEmail(String email);
}