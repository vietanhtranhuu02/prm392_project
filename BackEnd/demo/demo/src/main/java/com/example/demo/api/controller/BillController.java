package com.example.demo.api.controller;

import com.example.demo.api.model.BillBody;
import com.example.demo.entity.User;
import com.example.demo.service.Bill.BillService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/bill")
public class BillController {

    private final BillService billService;

    public BillController(BillService _billService) {
        this.billService = _billService;
    }

    @PostMapping
    public void createBill(@RequestBody BillBody body) {

        try {
            billService.createBill(body);
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
